package weather

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"
)

func Fetcher(location string, email string) http.HandlerFunc {
	weather := []byte{}

	var weatherIcons = map[string]string{
		"clear-day":           "🔆",
		"clear-night":         "🌙",
		"rain_showers":        "🌧",
		"snow":                "❄️",
		"sleet":               "🌨",
		"wind":                "⇝",
		"fog":                 "🌫",
		"haze":                "🌫",
		"cloudy":              "☁️",
		"partly-cloudy-day":   "⛅",
		"partly-cloudy-night": "☁️☽☁️",
		"hail":                "🌨",
		"thunderstorms":       "⛈️",
		"tornado":             "🌪",
		"smoke":               "💨",
	}

	lastWeather := time.Now()
	wmutex := &sync.RWMutex{}

	client := http.Client{}
	httpReq, _ := http.NewRequest("GET", "https://api.weather.gov/points/"+location, nil)
	httpReq.Header.Add("User-Agent", fmt.Sprintf("refugeapp %s", email))
	resp, _ := client.Do(httpReq)
	if resp.Body != nil {
		resp.Body.Close()
	}
	return func(w http.ResponseWriter, r *http.Request) {
		wmutex.RLock()
		if time.Now().Sub(lastWeather) < (time.Minute*15) && len(weather) > 0 {
			w.Write(weather)
			wmutex.RUnlock()
			return
		}
		wmutex.RUnlock()
		wmutex.Lock()
		defer wmutex.Unlock()

		// https://api.weather.gov/points/45.67,-111.14 -> properties.forecastGridData
		// https://api.weather.gov/gridpoints/TFX/93,58
		//		search for current time in:
		// 		properties.temperature.values[]
		//		properties.skyCover.values[]
		//		properties.windDirection.values[]
		//		properties.windSpeed.values[]
		//		properties.probabilityOfPrecipitation.values[]
		//		properties.lightningActivityLevel.values[]

		httpReq, _ := http.NewRequest("GET", `https://api.weather.gov/gridpoints/TFX/93,58`, nil)

		httpReq.Header.Add("User-Agent", fmt.Sprintf("refugeapp %s", email))
		resp, err := client.Do(httpReq)
		if err != nil {
			log.Printf("[Error] Failed to get weather data: %v", err)
			w.Write(weather)
			return
		}
		defer resp.Body.Close()

		var wd WGGridPoint
		err = json.NewDecoder(resp.Body).Decode(&wd)
		if err != nil {
			log.Printf("[Error] Failed to parse weather data response: %v", err)
			w.Write(weather)
			return
		}

		timeNow := time.Now().UTC()
		curTemp := searchClosestTime(wd.Properties.Temperature.Values, timeNow)
		tempInC := wd.Properties.Temperature.Uom == "wmoUnit:degC"
		if tempInC {
			curTemp.Value = CToF(curTemp.Value)
		}
		curHum := searchClosestTime(wd.Properties.RelativeHumidity.Values, timeNow)
		curWindSpeed := searchClosestTime(wd.Properties.WindSpeed.Values, timeNow)
		curWindDir := searchClosestTime(wd.Properties.WindDirection.Values, timeNow)
		curWeather, found := searchClosestWeather(wd.Properties.Weather.Values, timeNow)

		hours := ""
		st := time.Now().Truncate(time.Hour)
		for i := 0; i < 24; i++ {
			ampm := "am"
			hour := st.Add(time.Hour * time.Duration(i+1)).Hour()
			if hour > 12 {
				hour -= 12
				ampm = "pm"
			}
			hours += fmt.Sprintf("<td>%d%s</td>", hour, ampm)
		}
		temps := ""
		lastForecastTime := timeNow.Truncate(time.Hour)
		endTime := lastForecastTime.Add(time.Hour * 24)
		totalHours := 0
		for _, tempv := range wd.Properties.Temperature.Values {
			vtime := time.Time(tempv.ValidTime)

			if vtime.Before(lastForecastTime) || vtime.Equal(lastForecastTime) {
				continue
			}
			if vtime.After(endTime) {
				break
			}
			hours := int(vtime.Sub(lastForecastTime).Hours())
			if hours+totalHours > 24 {
				hours = 24 - totalHours
			}
			totalHours += hours
			if tempInC {
				tempv.Value = CToF(tempv.Value)
			}
			tmpStr := fmt.Sprintf("<td>%d</td>", int(tempv.Value))
			for i := 0; i < hours; i++ {
				temps += tmpStr
			}
			lastForecastTime = vtime
		}

		skys := ""
		for i := 0; i < 24; i++ {
			foreTime := timeNow.Add(time.Hour * time.Duration(i+1))
			sky := searchClosestTime(wd.Properties.SkyCover.Values, foreTime)
			thunder := searchClosestTime(wd.Properties.ProbabilityOfThunder.Values, foreTime)
			precip := searchClosestTime(wd.Properties.ProbabilityofPrecipitation.Values, foreTime)
			wind := searchClosestTime(wd.Properties.WindSpeed.Values, foreTime)
			windDir := searchClosestTime(wd.Properties.WindDirection.Values, foreTime)

			weatherIcon := ""
			const iconCutoff = 5
			if precip.Value > iconCutoff {
				weatherIcon += fmt.Sprintf("%d%%🌧", int(precip.Value))
			}
			if thunder.Value > iconCutoff {
				weatherIcon += fmt.Sprintf("%d%%⚡", int(precip.Value))
			}
			if precip.Value < iconCutoff && thunder.Value < iconCutoff && wind.Value > 15 {
				// if no other weather and wind is high, show wind icon
				weatherIcon += fmt.Sprintf(`<i style="transform:rotate(%ddeg);display: inline-block;">⇝</i>`, int(windDir.Value)-90)
			}

			skys += fmt.Sprintf(`
<td class="forecastCell">
	<div style="height: %d%%;"></div>
	<p>%s</p>
</td>`, int(sky.Value), weatherIcon)
		}
		forecast := fmt.Sprintf(`<table class="forecastTable">
		<tr>%s</tr>
		<tr style="height:25px">%s</tr>
		<tr>%s</tr>
		</table>`, hours, skys, temps)

		var wval string
		if found && len(curWeather.Value) > 0 {
			wval = curWeather.Value[0].GetWeather()
		}
		wicon, ok := weatherIcons[wval]
		if !ok {
			wicon = wval
		}

		weather = []byte(fmt.Sprintf(`
<span>%s</span>
<span>%d°🌡️</span>
<span>%d%%💧</span>
<span>%.1fmph<i style="transform:rotate(%ddeg);display: inline-block;">⇝</i></span>
<br>%s`, wicon, int(curTemp.Value), int(curHum.Value), curWindSpeed.Value, int(curWindDir.Value)-90, forecast))
		lastWeather = time.Now()
		w.Write(weather)
	}
}

func CToF(v float64) float64 {
	return (v * 1.8) + 32
}

func searchClosestWeather(values []WGWeather, now time.Time) (WGWeather, bool) {
	var bestValue WGWeather
	found := false
	for _, v := range values {
		newDist := now.Sub(time.Time(v.ValidTime)).Minutes()
		if newDist < 0 {
			break
		}
		bestValue = v
		found = true
	}
	return bestValue, found
}

func searchClosestTime(values []WGValue, now time.Time) WGValue {
	var bestValue WGValue
	for _, v := range values {
		newDist := now.Sub(time.Time(v.ValidTime)).Minutes()
		if newDist < 0 {
			break
		}
		bestValue = v
	}
	return bestValue
}

type WGGridPoint struct {
	Context  []interface{} `json:"@context"`
	ID       string        `json:"id"`
	Type     string        `json:"type"`
	Geometry struct {
		Type        string        `json:"type"`
		Coordinates [][][]float64 `json:"coordinates"`
	} `json:"geometry"`
	Properties WGProperties `json:"properties"`
}

type WGProperties struct {
	ID   string `json:"@id"`
	Type string `json:"@type"`
	// Updatetime time.Time `json:"updateTime"`
	// Validtimes time.Time `json:"validTimes"`
	Elevation struct {
		Unitcode string  `json:"unitCode"`
		Value    float64 `json:"value"`
	} `json:"elevation"`
	Forecastoffice          string       `json:"forecastOffice"`
	GridID                  string       `json:"gridId"`
	GridX                   string       `json:"gridX"`
	GridY                   string       `json:"gridY"`
	Temperature             WGUnitValues `json:"temperature"`
	Dewpoint                WGUnitValues `json:"dewpoint"`
	MaxTemperature          WGUnitValues `json:"maxTemperature"`
	MinTemperature          WGUnitValues `json:"minTemperature"`
	RelativeHumidity        WGUnitValues `json:"relativeHumidity"`
	ApparentTemperature     WGUnitValues `json:"apparentTemperature"`
	WetBulbGlobeTemperature WGUnitValues `json:"wetBulbGlobeTemperature"`
	HeatIndex               WGUnitValues `json:"heatIndex"`
	WindChill               WGUnitValues `json:"windChill"`
	SkyCover                WGUnitValues `json:"skyCover"`
	WindDirection           WGUnitValues `json:"windDirection"`
	WindSpeed               WGUnitValues `json:"windSpeed"`
	WindGust                WGUnitValues `json:"windGust"`
	Weather                 struct {
		Values []WGWeather `json:"values"`
	} `json:"weather"`
	Hazards                          WGHazards    `json:"hazards"`
	ProbabilityofPrecipitation       WGUnitValues `json:"probabilityOfPrecipitation"`
	QuantitativePrecipitation        WGUnitValues `json:"quantitativePrecipitation"`
	IceAccumulation                  WGUnitValues `json:"iceAccumulation"`
	SnowfallAmount                   WGUnitValues `json:"snowfallAmount"`
	SnowLevel                        WGUnitValues `json:"snowLevel"`
	CeilingHeight                    WGUnitValues `json:"ceilingHeight"`
	Visibility                       WGUnitValues `json:"visibility"`
	TransportWindSpeed               WGUnitValues `json:"transportWindSpeed"`
	TransportWindDirection           WGUnitValues `json:"transportWindDirection"`
	MixingHeight                     WGUnitValues `json:"mixingHeight"`
	HainesIndex                      WGUnitValues `json:"hainesIndex"`
	LightningActivityLevel           WGUnitValues `json:"lightningActivityLevel"`
	Twentyfootwindspeed              WGUnitValues `json:"twentyFootWindSpeed"`
	Twentyfootwinddirection          WGUnitValues `json:"twentyFootWindDirection"`
	Waveheight                       WGUnitValues `json:"waveHeight"`
	Waveperiod                       WGUnitValues `json:"wavePeriod"`
	Wavedirection                    WGUnitValues `json:"waveDirection"`
	Primaryswellheight               WGUnitValues `json:"primarySwellHeight"`
	Primaryswelldirection            WGUnitValues `json:"primarySwellDirection"`
	Secondaryswellheight             WGUnitValues `json:"secondarySwellHeight"`
	Secondaryswelldirection          WGUnitValues `json:"secondarySwellDirection"`
	Waveperiod2                      WGUnitValues `json:"wavePeriod2"`
	Windwaveheight                   WGUnitValues `json:"windWaveHeight"`
	Dispersionindex                  WGUnitValues `json:"dispersionIndex"`
	Pressure                         WGUnitValues `json:"pressure"`
	Probabilityoftropicalstormwinds  WGUnitValues `json:"probabilityOfTropicalStormWinds"`
	Probabilityofhurricanewinds      WGUnitValues `json:"probabilityOfHurricaneWinds"`
	Potentialof15Mphwinds            WGUnitValues `json:"potentialOf15mphWinds"`
	Potentialof25Mphwinds            WGUnitValues `json:"potentialOf25mphWinds"`
	Potentialof35Mphwinds            WGUnitValues `json:"potentialOf35mphWinds"`
	Potentialof45Mphwinds            WGUnitValues `json:"potentialOf45mphWinds"`
	Potentialof20Mphwindgusts        WGUnitValues `json:"potentialOf20mphWindGusts"`
	Potentialof30Mphwindgusts        WGUnitValues `json:"potentialOf30mphWindGusts"`
	Potentialof40Mphwindgusts        WGUnitValues `json:"potentialOf40mphWindGusts"`
	Potentialof50Mphwindgusts        WGUnitValues `json:"potentialOf50mphWindGusts"`
	Potentialof60Mphwindgusts        WGUnitValues `json:"potentialOf60mphWindGusts"`
	GrasslandFireDangerIndex         WGUnitValues `json:"grasslandFireDangerIndex"`
	ProbabilityOfThunder             WGUnitValues `json:"probabilityOfThunder"`
	DavisStabilityIndex              WGUnitValues `json:"davisStabilityIndex"`
	Atmosphericdispersionindex       WGUnitValues `json:"atmosphericDispersionIndex"`
	Lowvisibilityoccurrenceriskindex WGUnitValues `json:"lowVisibilityOccurrenceRiskIndex"`
	Stability                        WGUnitValues `json:"stability"`
	RedFlagThreatIndex               WGUnitValues `json:"redFlagThreatIndex"`
}

type WGWeather struct {
	ValidTime WGTime           `json:"validTime"`
	Value     []WGWeatherValue `json:"value"`
}

type WGTime time.Time

func (wgt *WGTime) UnmarshalJSON(data []byte) error {
	if len(data) < 3 {
		return nil
	}
	st := strings.Split(string(data[1:]), "/")[0]
	newTime, err := time.Parse(time.RFC3339, st)
	*wgt = WGTime(newTime)
	return err
}

type WGWeatherValue struct {
	Coverage   interface{} `json:"coverage"`
	Weather    *string     `json:"weather"`
	Intensity  interface{} `json:"intensity"`
	Visibility struct {
		Unitcode string      `json:"unitCode"`
		Value    interface{} `json:"value"`
	} `json:"visibility"`
	Attributes []interface{} `json:"attributes"`
}

func (wgv *WGWeatherValue) GetWeather() string {
	if wgv.Weather == nil {
		return ""
	}
	return *wgv.Weather
}

type WGUnitValues struct {
	Uom    string    `json:"uom"`
	Values []WGValue `json:"values"`
}

type WGValue struct {
	ValidTime WGTime  `json:"validTime"`
	Value     float64 `json:"value"`
}

type WGHazards struct {
	Values []WGHazardValue `json:"values"`
}

// https://www.weather.gov/bro/mapcolors
// WGHazardValue could be either [] or
//
//	{
//		"phenomenon": "HT",
//		"significance": "Y",
//		"event_number": null
//	}
type WGHazardValue struct {
	Value []map[string]interface{} `json:"value"`
}
