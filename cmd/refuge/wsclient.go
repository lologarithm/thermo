package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/lologarithm/refuge/refuge"
)

var upgrader = websocket.Upgrader{} // use default options

// Request is sent from websocket client to server to request change to someting
type Request struct {
	Name    string           // Name of device to update
	Climate *refuge.Settings // Climate Control Change Request
	Toggle  int              // Toggle of device request.
	Pos     *Position        // Request to change device position
}

// DeviceUpdate is a message to the client containing updated information about
// a particular device
type DeviceUpdate struct {
	*refuge.Device
	Pos        Position
	LastUpdate time.Time
}

// Position of a device in the UI
type Position struct {
	X, Y   int
	RoomID string
}

func (srv *server) clientStreamHandler(w http.ResponseWriter, r *http.Request) {
	access := auth(srv, w, r)
	if access == AccessNone {
		return
	}
	c := clientStream(w, r, access, srv)
	msgs := make([]*DeviceUpdate, 0, 10) // 10 seems like a reasonable number of devides.
	srv.datalock.Lock()
	for _, v := range srv.Devices {
		d := v.device
		msgs = append(msgs, &DeviceUpdate{Device: &d, Pos: v.pos, LastUpdate: v.lastUpdate})
	}
	srv.datalock.Unlock()
	for _, msg := range msgs {
		c.WriteJSON(msg)
	}
	srv.clientslock.Lock()
	srv.clientStreams = append(srv.clientStreams, &listener{wc: c})
	srv.clientslock.Unlock()
}

func clientStream(w http.ResponseWriter, r *http.Request, access int, srv *server) *websocket.Conn {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade failure:", err)
		return nil
	}

	// websocket reader closure.
	// Handles requests from websocket client.
	go func() {
		for {
			v := &Request{}
			err := c.ReadJSON(v)
			if err != nil {
				log.Println("Disconnecting user: ", err)
				break
			}
			log.Printf("Got client Request: %#v", v)
			// Readers can't write new settings
			if access != AccessWrite {
				continue
			}
			srv.handleClientRequest(v)
		}
		c.Close()
	}()
	return c
}
