package main

import (
	"encoding/json"
	"log"
	"net"
	"os"
)

type ConnRequest struct {
	User  string
	Pass  string
	Token string
	Addr  string
}

func (srv *server) handleConnection(conn net.Conn) {
	log.Printf("New Conn: %s", conn.RemoteAddr().String())

	encoder := json.NewEncoder(conn)
	decoder := json.NewDecoder(conn)

	cr := ConnRequest{}
	decoder.Decode(&cr)
	cr.Addr = conn.RemoteAddr().String()
	session := srv.validate(cr)
	if session.User.Access == AccessNone {
		encoder.Encode(map[string]interface{}{"error": "not authorized"})
		return
	}
	log.Printf("User: %#v, sending updates now", session)

	msgs := make([]*DeviceUpdate, 0, 10) // 10 seems like a reasonable number of devides.
	srv.datalock.Lock()
	for _, v := range srv.Devices {
		d := v.device
		msgs = append(msgs, &DeviceUpdate{Device: &d, Pos: v.pos, LastUpdate: v.lastUpdate})
	}
	srv.datalock.Unlock()
	for _, msg := range msgs {
		encoder.Encode(msg)
	}

	srv.clientslock.Lock()
	srv.clientStreams = append(srv.clientStreams, &listener{conn: conn})
	srv.clientslock.Unlock()

	for {
		v := &Request{}
		err := decoder.Decode(v)
		if err != nil {
			log.Println("Disconnecting user: ", err)
			break
		}
		log.Printf("Got client Request: %#v", v)
		// Readers can't write new settings
		if session.User.Access != AccessWrite {
			continue
		}
		srv.handleClientRequest(v)
	}
	conn.Close()
}

func (srv *server) handleClientRequest(v *Request) {
	dev := srv.getDevice(v.Name)
	if v.Pos != nil {
		d, _ := json.Marshal(v.Pos)
		os.WriteFile("./pos/"+dev.device.Name+".pos", d, 0644)
		dev.pos = *v.Pos
	} else if v.Climate != nil {
		setTherm(*v.Climate, srv.conn, dev.addr)
	} else if v.Toggle > 0 {
		if dev.device.Switch != nil {
			toggleSwitch(v.Toggle, srv.conn, dev.addr)
		}
		if dev.device.Portal != nil {
			togglePortal(v.Toggle, srv.conn, dev.addr)
		}
	}
}
