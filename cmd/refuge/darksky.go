package main

type DarkSkyWeather struct {
	Latitude  float64      `json:"latitude"`
	Longitude float64      `json:"longitude"`
	Timezone  string       `json:"timezone"`
	Currently DSDataPoint  `json:"currently"`
	Minutely  *DSDataBlock `json:"minutely"`
	Hourly    *DSDataBlock `json:"hourly"`
	Daily     *DSDataBlock `json:"daily"`
	Flags     struct {
		Sources        []string `json:"sources"`
		NearestStation float64  `json:"nearest-station"`
		Units          string   `json:"units"`
	} `json:"flags"`
	Offset int        `json:"offset"`
	Alerts []*DSAlert `json:"alerts"`
}

type DSDataBlock struct {
	Summary string        `json:"summary"`
	Icon    string        `json:"icon"`
	Data    []DSDataPoint `json:"data"`
}

type DSDataPoint struct {
	ApparentTemperature         float64 `json:"apparentTemperature"`
	ApparentTemperatureHigh     float64 `json:"apparentTemperatureHigh"`
	ApparentTemperatureHighTime int64   `json:"apparentTemperatureHighTime"`
	ApparentTemperatureLow      float64 `json:"apparentTemperatureLow"`
	ApparentTemperatureLowTime  int64   `json:"apparentTemperatureLowTime"`
	ApparentTemperatureMax      float64 `json:"apparentTemperatureMax"`
	ApparentTemperatureMaxTime  int64   `json:"apparentTemperatureMaxTime"`
	ApparentTemperatureMin      float64 `json:"apparentTemperatureMin"`
	ApparentTemperatureMinTime  int64   `json:"apparentTemperatureMinTime"`
	CloudCover                  float64 `json:"cloudCover"`
	DewPoint                    float64 `json:"dewPoint"`
	Humidity                    float64 `json:"humidity"`
	Icon                        string  `json:"icon"`
	MoonPhase                   float64 `json:"moonPhase"`
	NearestStormBearing         float64 `json:"nearestStormBearing"`
	NearestStormDistance        float64 `json:"nearestStormDistance"`
	Ozone                       float64 `json:"ozone"`
	PrecipAccumulation          float64 `json:"precipAccumulation"`
	PrecipIntensity             float64 `json:"precipIntensity"`
	PrecipIntensityError        float64 `json:"precipIntensityError"`
	PrecipIntensityMax          float64 `json:"precipIntensityMax"`
	PrecipIntensityMaxTime      int64   `json:"precipIntensityMaxTime"`
	PrecipProbability           float64 `json:"precipProbability"`
	PrecipType                  string  `json:"precipType"`
	Pressure                    float64 `json:"pressure"`
	Summary                     string  `json:"summary"`
	SunriseTime                 int64   `json:"sunriseTime"`
	SunsetTime                  int64   `json:"sunsetTime"`
	Temperature                 float64 `json:"temperature"`
	TemperatureHigh             float64 `json:"temperatureHigh"`
	TemperatureHighTime         int64   `json:"temperatureHighTime"`
	TemperatureLow              float64 `json:"temperatureLow"`
	TemperatureLowTime          int64   `json:"temperatureLowTime"`
	TemperatureMax              float64 `json:"temperatureMax"`
	TemperatureMaxTime          int64   `json:"temperatureMaxTime"`
	TemperatureMin              float64 `json:"temperatureMin"`
	TemperatureMinTime          int64   `json:"temperatureMinTime"`
	Time                        int64   `json:"time"`
	UvIndex                     int64   `json:"uvIndex"`
	UvIndexTime                 int64   `json:"uvIndexTime"`
	Visibility                  float64 `json:"visibility"`
	WindBearing                 float64 `json:"windBearing"`
	WindGust                    float64 `json:"windGust"`
	WindGustTime                int64   `json:"windGustTime"`
	WindSpeed                   float64 `json:"windSpeed"`
}

type DSAlert struct {
	Description string   `json:"description"`
	Expires     int64    `json:"expires"`
	Regions     []string `json:"regions"`
	Severity    string   `json:"severity"`
	Time        int64    `json:"time"`
	Title       string   `json:"title"`
	URI         string   `json:"uri"`
}
