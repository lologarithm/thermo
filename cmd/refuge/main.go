package main

import (
	"flag"
	"net"

	"gitlab.com/lologarithm/refuge/rnet"
)

func main() {
	host := flag.String("host", ":80", "host:port to serve on")
	test := flag.Bool("test", false, "use fake test data so no network is needed.")
	flag.Parse()

	// Setup user access
	loadUserConfig()

	// Launcher monitors and serves web host.
	var monitorNotices chan rnet.Msg
	var conn *net.UDPConn
	if *test {
		monitorNotices = fakeMonitor()
	} else {
		monitorNotices, conn = networkMonitor()
	}

	serve(*host, runServer(monitorNotices, conn))
}
