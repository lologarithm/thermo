package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/gorilla/websocket"
	_ "github.com/influxdata/influxdb1-client" // this is important because of the bug in go mod
	client "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/lologarithm/refuge/cmd/refuge/weather"
	"gitlab.com/lologarithm/refuge/refuge"
	"gitlab.com/lologarithm/refuge/rnet"
)

type userAccess struct {
	Name   string
	Pwd    string
	Access int
}

// Access levels
const (
	AccessNone  int = 0
	AccessRead      = 1
	AccessWrite     = 2
)

type server struct {
	// Server Data
	datalock *sync.RWMutex
	Devices  map[string]*refugeDevice

	// Update Streams
	deviceStream chan rnet.Msg
	devUpdates   chan refuge.Device

	// web clients
	clientslock   *sync.RWMutex
	clientStreams []*listener
	sessions      map[string]session

	// Internal Network
	conn *net.UDPConn

	done chan struct{}
}

type listener struct {
	wc   *websocket.Conn
	conn net.Conn
}

func (l *listener) write(b []byte) error {
	if l.wc != nil {
		return l.wc.WriteMessage(websocket.TextMessage, b)
	}
	if l.conn != nil {
		_, err := l.conn.Write(b)
		return err
	}
	return errors.New("unable to find a connection")
}

func (srv *server) NewSession(access userAccess) string {
	sess := session{Expires: time.Now().AddDate(0, 0, 7), User: access}
	id := randString(32)
	srv.clientslock.Lock()
	srv.sessions[id] = sess
	srv.clientslock.Unlock()
	return id
}

var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// pulled from https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go
func randString(n int) string {
	sb := strings.Builder{}
	sb.Grow(n)
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return sb.String()
}

func (srv *server) GetSession(id string) session {
	srv.clientslock.RLock()
	sess, ok := srv.sessions[id]
	srv.clientslock.RUnlock()

	// Clean old sessions
	if ok && !sess.Expires.After(time.Now()) {
		srv.clientslock.Lock()
		delete(srv.sessions, id)
		srv.clientslock.Unlock()
	}
	return sess
}

type session struct {
	Expires time.Time
	Token   string
	User    userAccess
}

func runServer(deviceStream chan rnet.Msg, udpConn *net.UDPConn) *server {
	srv := &server{
		datalock:     &sync.RWMutex{},
		Devices:      map[string]*refugeDevice{},
		deviceStream: deviceStream,
		clientslock:  &sync.RWMutex{},
		sessions:     map[string]session{},
		done:         make(chan struct{}, 1),
		devUpdates:   make(chan refuge.Device, 5), // Updates from network -> portal watcher
		conn:         udpConn,
	}
	go portalAlert(globalConfig, srv.devUpdates, udpConn)
	// Updater goroutine. Updates data state and pushes the new state to websocket clients
	go eventListener(srv, deviceStream)
	return srv
}

// Getter functions convert names to device keys (avoiding spaces)

func (srv *server) getDevice(name string) (device *refugeDevice) {
	name = strings.Replace(name, " ", "", -1)
	if name == "" {
		log.Printf("[Error] getDevice: Attempted to fetch an empty name string!")
		return nil
	}
	srv.datalock.Lock()
	device = srv.Devices[name]
	srv.datalock.Unlock()
	return device
}
func (srv *server) stop() {
	close(srv.deviceStream)
	close(srv.devUpdates)
	<-srv.done
}

type refugeDevice struct {
	device refuge.Device
	// conn   *net.UDPConn
	addr       *net.UDPAddr
	pos        Position
	lastUpdate time.Time
}

// serve creates the state object "server" and http handlers and launches the http listener.
// Blocks on ctrl+c
func serve(host string, srv *server) {
	// Little weather proxy/cache for the frontends
	http.HandleFunc("/weather", weather.Fetcher(globalConfig.Location, globalConfig.WeatherEmail))
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) { w.WriteHeader(http.StatusOK) })
	http.HandleFunc("/stream", srv.clientStreamHandler)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if auth(srv, w, r) == AccessNone {
			return // Don't let them access
		}
		path := strings.Split(strings.TrimPrefix(r.URL.Path, "/"), "/")
		switch path[0] {
		case "static", "assets":
			if path[1] == "house.html" {
				break
			}
			Static(w, r, path)
			return
		}
		// Default to base site.
		tmpl, err := template.ParseFiles("./assets/house.html")
		if err != nil {
			log.Fatalf("unable to parse html: %s", err)
		}
		tmpl.Execute(w, nil)

	})

	go func() {
		ln, err := net.Listen("tcp", "0.0.0.0:6907")
		if err != nil {
			// handle error
			log.Printf("Failed to listen to network: %s", err)
		}
		for {
			conn, err := ln.Accept()
			if err != nil {
				// handle error
				log.Printf("Failed to accept connection: %s", err)
			}
			go srv.handleConnection(conn)
		}
	}()

	log.Printf("starting webhost on: %s", host)
	go func() {
		err := http.ListenAndServe(host, nil)
		if err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	termInput := make(chan string, 1)
	buffer := bufio.NewReader(os.Stdin)
	go func() {
		for {
			line, _, err := buffer.ReadLine()
			if err != nil {
				log.Printf("[Error] Read Input: %s. No terminal input available.", err)
				return
			}
			termInput <- string(line)
		}
	}()

	for {
		select {
		case <-c:
			log.Printf("Exiting!")
			srv.stop() // wait for server to stop
			log.Printf("Done.")
			return
		case input := <-termInput:
			if len(input) == 0 {
				break // nothing to parse
			}
			switch input[0] {
			case 'i':
				srv.clientslock.RLock()
				log.Printf("%#v", srv.sessions)
				srv.clientslock.RUnlock()
			}
		}
	}
}

// Static will serve the given file from the path in the url.
func Static(w http.ResponseWriter, r *http.Request, path []string) {
	if len(path) < 2 || !strings.Contains(path[len(path)-1], ".") {
		// Only serve actual files here.
		return
	}
	file := strings.Join(path, "/")
	if strings.Contains(file, "/gz/") {
		w.Header().Set("Content-Encoding", "gzip")
	}
	http.ServeFile(w, r, file)
}

// eventListener listens to events from the deviceStream (network).
// It will update all currently listening websockets
// It pushes the events then to the monitoring system.
func eventListener(srv *server, deviceStream chan rnet.Msg) {
	influxClient, err := client.NewHTTPClient(client.HTTPConfig{
		Addr: "http://localhost:8086",
	})
	if err != nil {
		panic(err) // error handling here; normally we wouldn't use fmt but it works for the example
	}

	for {
		msg, ok := <-deviceStream
		if !ok {
			srv.done <- struct{}{}
			return
		}
		td := msg.Device
		existing := srv.getDevice(td.Name)
		newd := &refugeDevice{
			device:     *td,
			lastUpdate: time.Now(),
		}
		if existing != nil {
			newd.pos = existing.pos
			if existing.device.Addr != td.Addr {
				log.Printf("[INFO] Device '%s' has changed address from '%s' to '%s'", existing.device.Name, existing.device.Addr, td.Addr)
				raddr, err := net.ResolveUDPAddr("udp", td.Addr)
				if err != nil {
					log.Printf("Failed to resolve UDP addr for device (%#v): %s", existing.device, err)
					continue
				}
				newd.addr = raddr
			} else {
				newd.addr = existing.addr
			}
		} else {
			raddr, err := net.ResolveUDPAddr("udp", td.Addr)
			if err != nil {
				log.Printf("Failed to resolve UDP addr for device (%#v): %s", existing.device, err)
				continue
			}
			newd.addr = raddr

			fdata, err := ioutil.ReadFile("./pos/" + td.Name + ".pos")
			if err == nil {
				pos := &Position{}
				json.Unmarshal(fdata, pos)
				newd.pos = *pos
			}
		}

		id := strings.Replace(td.Name, " ", "", -1)
		if newd.device.Thermostat != nil {
			dowrite := true
			if dev, ok := srv.Devices[id]; ok {
				if *dev.device.Thermometer == *newd.device.Thermometer &&
					dev.device.Thermostat.State == newd.device.Thermostat.State {
					dowrite = false // only record changes
				}
			}

			if dowrite {
				bp, _ := client.NewBatchPoints(client.BatchPointsConfig{
					Database: "refuge",
				})
				p, err := client.NewPoint("thermostat", map[string]string{},
					map[string]interface{}{"name": id, "temp": newd.device.Thermometer.Temp, "humidity": newd.device.Thermometer.Humidity, "state": int(newd.device.Thermostat.State)},
					time.Now())
				if err != nil {
					log.Fatalf("[Error] stats writer failed to create stat point: %s", err)
				}
				bp.AddPoint(p)
				influxClient.Write(bp)
			}
		}

		// Update our cached thermostat
		srv.datalock.Lock()
		srv.Devices[id] = newd
		srv.datalock.Unlock()
		srv.devUpdates <- *td // push updates to alert system

		up := &DeviceUpdate{
			Device:     &newd.device,
			Pos:        newd.pos,
			LastUpdate: newd.lastUpdate,
		}
		// Serialize for clients
		d, err := json.Marshal(up)
		if err != nil {
			log.Printf("[Error] Failed to marshal thermal data to json: %s", err)
		}

		// Now push the update to all connected websockets
		deadstreams := []int{}
		srv.clientslock.Lock()
		for i, cs := range srv.clientStreams {
			err := cs.write(d)
			if err != nil {
				deadstreams = append(deadstreams, i)
			}
		}
		// remove dead streams now
		for i := len(deadstreams) - 1; i > -1; i-- {
			idx := deadstreams[i]
			srv.clientStreams = append(srv.clientStreams[:idx], srv.clientStreams[idx+1:]...)
		}
		srv.clientslock.Unlock()
	}
}

func getTodayDate() time.Time {
	now := time.Now()
	year, month, day := now.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, now.Location())
}

func auth(srv *server, w http.ResponseWriter, r *http.Request) int {
	addr := r.RemoteAddr
	if paddr := r.Header.Get("X-Echols-A"); paddr != "" {
		addr = paddr
	} else if paddr := r.Header.Get("X-Forwarded-For"); paddr != "" {
		addr = paddr
	}

	cr := ConnRequest{
		Addr: addr,
	}

	name, pwd, ok := r.BasicAuth()
	if ok {
		cr.User = name
		cr.Pass = pwd
	}

	if refCookie, err := r.Cookie("refuge"); err == nil && refCookie != nil {
		cr.Token = refCookie.Value
	}

	session := srv.validate(cr)
	if session.User.Access == AccessNone {
		http.SetCookie(w, &http.Cookie{Name: "refuge", Value: "", Expires: time.Time{}})
		w.Header().Set("WWW-Authenticate", `Basic realm="Refuge"`)
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("No Access"))
		// clear cookie just in case.
		http.SetCookie(w, &http.Cookie{Name: "refuge", Value: session.Token, Expires: time.Time{}})
	} else if session.Token != "" {
		http.SetCookie(w, &http.Cookie{Name: "refuge", Value: session.Token, Expires: session.Expires})
	}
	return session.User.Access
}

func (srv *server) validate(cr ConnRequest) session {
	if cr.Token != "" {
		// Has existing auth cookie
		sess := srv.GetSession(cr.Token)
		if sess.Expires.After(time.Now()) && sess.User.Access != AccessNone {
			return sess
		}
	}

	user, ok := globalConfig.Users[cr.User]

	if strings.HasPrefix(cr.Addr, globalConfig.NetPrefix) || strings.HasPrefix(cr.Addr, "127.0.0.1") || strings.HasPrefix(cr.Addr, "[::1]") {
		// internal users always granted write access
		log.Printf("(%s) granted access as internally connected.", cr.Addr)
		user = userAccess{Access: AccessWrite}
	} else if !ok || user.Pwd != cr.Pass {
		log.Printf("(%s) has incorrect password.", cr.Addr)
		return session{}
	}

	// Now give this new session an ID
	id := srv.NewSession(user)
	return session{Expires: srv.sessions[id].Expires, User: user, Token: id}
}
