#!/bin/bash

# copies the binary for each device up as 'new'
# and then executes the deploy script to copy and restart service.

scp garage pi@192.168.1.55:~/garagenew
ssh pi@192.168.1.55 deploy.sh

scp switch pi@192.168.1.56:~/switchnew
ssh pi@192.168.1.56 deploy.sh

scp thermo pi@192.168.1.54:~/thermonew
ssh pi@192.168.1.54 deploy.sh

scp thermo pi@192.168.1.57:~/thermonew
ssh pi@192.168.1.57 deploy.sh

scp thermo pi@192.168.1.62:~/thermonew
ssh pi@192.168.1.62 deploy.sh
