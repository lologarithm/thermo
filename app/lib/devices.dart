class UpdateRequest {
  String name;
  Thermostat? climate;
  int? toggle;

  UpdateRequest(this.name);

  Map toJson() => {
        'name': name,
        'climate': climate?.toJson(),
        'toggle': toggle,
      };
}

class DeviceUpdate {
  String name;
  String addr;
  String id;
  DateTime lastUpdate;

  bool pending = false; // used internally

  // List of things that the device could have
  Switchable? switchable;
  Thermostat? thermostat;
  Thermometer? thermometer;
  Portal? portal;

  DeviceUpdate(this.name, this.addr, this.id, this.lastUpdate, {this.thermostat, this.thermometer, this.portal, this.switchable});

  factory DeviceUpdate.clone(DeviceUpdate original) {
    var dev = DeviceUpdate(original.name, original.addr, original.id, original.lastUpdate);
    if (original.switchable != null) {
      dev.switchable = Switchable.clone(original.switchable!);
    }
    if (original.thermostat != null) {
      dev.thermostat = Thermostat.clone(original.thermostat!);
    }
    if (original.thermometer != null) {
      dev.thermometer = Thermometer.clone(original.thermometer!);
    }
    if (original.portal != null) {
      dev.portal = Portal.clone(original.portal!);
    }

    return dev;
  }

  factory DeviceUpdate.fromJson(dynamic json) {
    return DeviceUpdate(json['Name'] as String, json['Addr'] as String, json['ID'] as String, DateTime.parse(json['LastUpdate'] as String),
        switchable: json["Switch"] != null ? Switchable.fromJson(json["Switch"]) : null,
        thermostat: json["Thermostat"] != null ? Thermostat.fromJson(json["Thermostat"]) : null,
        thermometer: json["Thermometer"] != null ? Thermometer.fromJson(json["Thermometer"]) : null,
        portal: json["Portal"] != null ? Portal.fromJson(json['Portal']) : null);
  }

  @override
  String toString() {
    return '{ $name }';
  }
}

class Switchable {
  bool on;

  Switchable(this.on);

  factory Switchable.clone(Switchable original) {
    return Switchable(original.on);
  }

  factory Switchable.fromJson(dynamic json) {
    return Switchable(json["On"]);
  }
}

class Portal {
  PortalState state;

  Portal(this.state);

  factory Portal.clone(Portal original) {
    return Portal(original.state);
  }

  factory Portal.fromJson(dynamic json) {
    return Portal(PortalState.values[json["State"]]);
  }
}

enum PortalState { unknown, closed, open }

class Thermostat {
  ThermostatState state;
  double target;

  double low;
  double high;
  ThermostatRunMode mode;

  Thermostat(this.state, this.target, this.low, this.high, this.mode);

  factory Thermostat.clone(Thermostat original) {
    return Thermostat(original.state, original.target, original.low, original.high, original.mode);
  }

  factory Thermostat.fromJson(dynamic json) {
    return Thermostat(ThermostatState.values[json["State"]], (json["Target"] as num).toDouble(), (json["Settings"]["Low"] as num).toDouble(),
        (json["Settings"]["High"] as num).toDouble(), ThermostatRunMode.values[json["Settings"]["Mode"]]);
  }

  Map toJson() => {
        'target': target,
        'low': low,
        'high': high,
      };
}

enum ThermostatState { idle, cool, fan, heat }

enum ThermostatRunMode { unset, off, auto, fan }

class Thermometer {
  double temp;
  double humidity;

  Thermometer(this.temp, this.humidity);

  factory Thermometer.clone(Thermometer original) {
    return Thermometer(original.temp, original.humidity);
  }

  factory Thermometer.fromJson(dynamic json) {
    return Thermometer((json["Temp"] as num).toDouble(), (json["Humidity"] as num).toDouble());
  }
}
