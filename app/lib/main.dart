import 'package:flutter/material.dart';
import 'root_page.dart';
import 'house_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    const refugeColor = Color.fromARGB(255, 96, 125, 132);

    return MaterialApp(
      title: 'Refuge',
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        colorScheme: ColorScheme.fromSeed(
            brightness: Brightness.dark, seedColor: refugeColor),
        useMaterial3: true,
      ),
      theme: ThemeData(
        brightness: Brightness.light,
        colorScheme: ColorScheme.fromSeed(seedColor: refugeColor),
        useMaterial3: true,
      ),
      themeMode: ThemeMode.light,
      home: RootPage(title: 'Refuge'),
    );
  }
}
